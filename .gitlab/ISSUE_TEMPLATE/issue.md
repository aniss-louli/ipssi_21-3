Do the checklist before filing an issue:

- [ ] Is this something you can **debug and fix**? Send a pull request!
- [ ] Have a usage question? 
- [ ] Have a proposition ?