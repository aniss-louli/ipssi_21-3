start:
	docker-compose up -d

stop:
	docker-compose down

build:
	docker-compose build

running:
	docker-compose ps

help:
	docker-compose --help